import 'bootstrap/dist/css/bootstrap.css'
import Head from 'next/head'
import Link from 'next/link'
import React, { useEffect, useState } from 'react';
import { address, abi } from '../nft_contract';
import Web3 from 'web3';


export default function App() {
  const [manager, setManager] = useState('');
  const [mintAmount, setMintAmount] = useState('');
  const [eth, setEth] = useState('');
  const [message, setMessage] = useState('');
  const [accounts, setAccounts] = useState('');
  const [web3, setWeb3] = useState('');
  const [contract, setContract] = useState('');

  useEffect(() => {
    window.ethereum.enable();
    let web3 = new Web3(window.web3.currentProvider);
    setWeb3(web3)
    let contract = new web3.eth.Contract(abi, address);
    setContract(contract)
    const init = async () => {
      const manager = await contract.methods.owner().call();
      setManager(manager);
    };
    init();
  }, []);

  const getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
  };

  const submitForm = async e => {
    e.preventDefault();
    if (accounts != null) {
      setMessage('Waiting on transaction success...' + accounts[0] + ' sending eth: ' + eth);
      const response = await contract.methods.mint(
        mintAmount
      ).send({
        from: accounts[0],
        value: web3.utils.toWei(eth, 'ether'),
      });
      setMessage('You have been entered! ' + response);
    }
  };

  const connectWallet = async e => {
    e.preventDefault();

    const accounts = await web3.eth.getAccounts();
    setAccounts(accounts)
  };


  return (
    <div className="container">
      <Head>
        <title>NFT Sample</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <h2>contract</h2>
      <p>This contract is managed by {manager}</p>
      <hr />
      <form onSubmit={connectWallet}>
        <div>
          <h4>wallet</h4>
          <p>{accounts[0]}</p>
          <button className='btn btn-primary'>Wallet</button>
        </div>
      </form>
      <form onSubmit={submitForm} >
        <h4></h4>
        <div>
          <div>
            <label>Amount of mint</label>
            <input
              style={{ marginLeft: '1vw' }}
              value={mintAmount}
              onChange={(e) => setMintAmount(e.target.value)}
            />
          </div>
          <div
            style={{ marginTop: '1vw' }}
          >
            <label>Amount of eth</label>
            <input
              style={{ marginLeft: '1vw' }}
              value={eth}
              onChange={(e) => setEth(e.target.value)}
            />
          </div>
          <button className='btn btn-primary'>Enter</button>
        </div>
      </form>
      <footer>{message}</footer>
      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
        }

        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }

        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }

        .logo {
          height: 1em;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}