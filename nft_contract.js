
const address = process.env.REACT_APP_CONTRACT_ADDRESS;

const abi = [
    {
        constant: true,
        inputs: [],
        name: 'owner',
        outputs: [{ name: '', type: 'address' }],
        payable: false,
        stateMutability: 'view',
        type: 'function',
    },
    {
        constant: false,
        inputs: [
            { name: 'mintCount', type: 'uint256' }
        ],
        name: 'mint',
        outputs: [],
        payable: true,
        stateMutability: 'payable',
        type: 'function',
    },
    {
        inputs: [],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'constructor',
    },
];

module.exports = {
    address: address,
    abi: abi
}

// @ts-ignore
// export default new web3.eth.Contract(abi, address);
